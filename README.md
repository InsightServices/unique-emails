# Unique Emails

## Instructions
1. Clone this repository to your machine (in ~/Source/Repos) using GIT bash
2. Create a branch for your changes
3. Modify the `NumUniqueEmails()` method so that the unit test passes
4. Commit your working solution and push your branch to BitBucket
5. Create a pull request explaining your changes

* You should be able to solve this problem without using LINQ.  If you wish, you can implement both a non-LINQ and LINQ solution.
* Using VS Code, use the `dotnet build` and `dotnet test` terminal commands to build and test your code, respectively.

## Details
Every email consists of a local name and a domain name, separated by the `@` sign.

For example, in `Alice@testoil.com`, `Alice` is the local name, and `testoil.com` is the domain name.

Besides lowercase letters, these emails may contain `.` or `+`.

If you add periods (`.`) between some characters in the local name part of an email address, mail sent there will be forwarded to the same address without dots in the local name.  For example, `Alice.M@testoil.com` and `AliceM@testoil.com` forward to the same email address.  (Note that this rule does not apply for domain names.)

If you add a plus (`+`) in the local name, everything after the first plus sign will be ignored. This allows certain emails to be filtered, for example `m.y+name@email.com` will be forwarded to `my@email.com`.  (Again, this rule does not apply for domain names.)

It is possible to use both of these rules at the same time.

Given a list of `emails`, we send one email to each address in the list.  How many different addresses actually receive mails? 

## Example
```
Input: ["test.email+alex@testoil.com","test.e.mail+bob.cathy@testoil.com","TestEmail+david@tes.toil.com"]
Output: 2
```
Explanation: `TestEmail@testoil.com` and `TestEmail@tes.toil.com` actually receive mails

## Note
* Each `emails[i]` contains exactly one `@` character