namespace Test
{
    using App;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Shouldly;

    [TestClass]
    public class UniqueEmailTests
    {
        // This code tests your implementation of NumUniqueEmails().
        // Do not modify this method.
        [TestMethod]
        public void TestMethod1()
        {
            var checker = new UniqueEmail();

            string[] input = new string[] { "test.email+alex@testoil.com","test.e.mail+bob.cathy@testoil.com","testemail+david@test.oil.com"};

            checker.NumUniqueEmails(input).ShouldBe(2);
        }
    }
}
